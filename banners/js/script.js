const imagesWrapper = document.querySelector(".images-wrapper");
const images = imagesWrapper.querySelectorAll("img");
const sliderTimeout = 3000;
const time = document.createElement("div");

let activeIndex = 0;
let bannersTimer = null;
let countDownTimer = null;
let startDate = Date.now();

const slider = function () {
    countDownTimer = setInterval(function () {
        time.innerText = String(sliderTimeout - (Date.now() - startDate));
    }, 25);

    bannersTimer = setInterval(function () {
        let nextIndex = activeIndex + 1;
        if (nextIndex >= images.length) {
            nextIndex = 0;
        }

        fadeOutFadeIn(images[activeIndex], images[nextIndex]);
        activeIndex++;

        if (activeIndex >= images.length) {
            activeIndex = 0;
        }
        startDate = Date.now();
    }, sliderTimeout);
};

function fadeOutFadeIn(current, next) {
    if (!current.style.opacity) {
        current.style.opacity = "1";
    }
    const fadeTimer = setInterval(function () {
        if (current.style.opacity > 0) {
            current.style.opacity -= 0.05;
        } else {
            current.classList.remove("active");
            current.style.opacity = 1;
            fadeIn(next);
            clearInterval(fadeTimer);
        }
    }, 30);
}

function fadeIn(element) {
    element.style.opacity = String(0);
    element.classList.add("active");
    const fadeInTimer = setInterval(function () {
        if (Number(element.style.opacity) <= 1) {
            element.style.opacity = String(Number(element.style.opacity) + Number(0.05));
        } else {
            clearInterval(fadeInTimer);
        }
    }, 30);
}

const pauseButton = document.createElement("button");
pauseButton.innerText = "Припинити";

pauseButton.addEventListener("click", function (e) {
    clearInterval(bannersTimer);
    clearInterval(countDownTimer);
    bannersTimer = null;
    this.disabled = true;
    resumeButton.disabled = false;
});

const resumeButton = document.createElement("button");
resumeButton.innerText = "Віндновити показ";
resumeButton.disabled = true;

resumeButton.addEventListener("click", function (e) {
    if (bannersTimer !== null) {
        return;
    }
    this.disabled = true;
    pauseButton.disabled = false;
    startDate = Date.now();
    slider();
});

document.addEventListener("DOMContentLoaded", function () {
    slider();
    imagesWrapper.insertAdjacentElement("afterend", pauseButton);
    imagesWrapper.insertAdjacentElement("afterend", resumeButton);
    document.body.insertAdjacentElement("beforeend", time);
});
